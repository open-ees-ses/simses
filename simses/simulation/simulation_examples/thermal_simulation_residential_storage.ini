; In this example config file, we will take a look at how to setup a simulation for a residential PV + BESS.
; This file specifies the configuration for a 10 kW_p PV system coupled with a 5 kW / 5 kWh BESS

; Section General:
; This section defines your general settings.
;   START: Start time of the simulation
;   END: End time of the simulation
;       Please make sure to use a profile that matches with the simulation time.
;   TIME_STEP: Simulation time step in s
;   LOOP: Number of simulation cycles (e.g. Loop = 2. The simulation runs with the selected settings twice.
;       The parameters of the storage system (e.g. degradation_model) continues.
;   EXPORT_DATA: Select if data is meant to be exported. If False is chosen, no data will be exported or logged and
;       you may not see any simulation results.
[GENERAL]
START = 2014-01-01 00:00:00
END = 2014-01-10 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_DATA = True

; Now that the simulation period and sample time for our simulation has been specified, we specify which algorithm is to
; be used for the Energy Management.
; We look at two algorithms in this example: ResidentialPvGreedy and ResidentialPvFeedInDamp.
; Please comment out line 36 if you wish to select the former strategy, and line 35 if vice-versa.
; Section Energy Management:
; This section defines your operation strategy / application.
;   STRATEGY: desired application (Single Use or Multi Use).
;       Please make sure to use a load, generation or technical profile that matches with the application.
;       E.g.: For a PV Home Storage application you need a PV-profile and a load profile
;       ResidentialPvGreedy: A residential storage with a PV-system is simulated (greedy strategy)
;       ResidentialPvFeedInDamp: A residential storage with a PV-system is simulated (feed-in damp strategy)
;       MIN_SOC: Lower SOC limit for the specified operation strategy in p.u [0,1[
;       MAX_SOC: Upper SOC limit for the specified operation strategy in p.u ]0,1]
[ENERGY_MANAGEMENT]
STRATEGY = ResidentialPvGreedy
;STRATEGY = ResidentialPvFeedInDamp
MIN_SOC = 0.0
MAX_SOC = 1.0

; Having settled for the energy management strategy of interest, we specify some basic battery parameters.
; This section describes data specific parameters. The following parameters are defined generally for all applications:
;   START_SOC: SOC at simulation start in p.u [0,1]
;   MIN_SOC: Lower SOC limit for the specified technology in p.u [0,1[
;   MAX_SOC: Upper SOC limit for the specified technology in p.u ]0,1]

; Battery specific parameter
;   EOL: End of life criteria in p.u [0, 1]. If the state of health of the used battery is reached,
;   the battery will be replaced
;   START_SOH_SHARE: Share of start SOH between calendar and cyclic degradation for both, capacity decrease and
;                    resistance increase
;   EXACT_SIZE: Allow non-integer values for cell serial and parallel connection (e.g. 22.2s2.7p) to size the battery
;   exactly for the defined energy rating instead of rounding to nearest neighbour (e.g. 22s3p).
[BATTERY]
START_SOC = 0.0
MIN_SOC = 0.0
MAX_SOC = 1.0
EOL = 0.6
START_SOH = 1
START_SOH_SHARE = 0.5
EXACT_SIZE = False

; We now specify the configuration details for our BESS at each level of the hierarchy.
[STORAGE_SYSTEM]
; We specify here that our BESS  consists of 1 ACSystem with a power rating of 5 kW, an intermediate DC-Voltage of 333 V,
; and which does not have any housing type or a dedicated HVAC system connected to it.
; Configuration of the AC storage system:
; Format: AC-system name, max AC power in W, DC voltage level in V, ACDC converter name, housing name, HVAC name
STORAGE_SYSTEM_AC =
    ac_system_1,5e3,333,acdc_1,no_housing,no_hvac

; We now specify the type of AC-DC Converter to be installed in our BESS.
; Configuration of the AC/DC converter:
; Format: ACDC converter name, converter type, optional: number of converters
ACDC_CONVERTER =
    acdc_1,NottonAcDcConverter

; We now specify the type of Housing to be installed in our BESS. In this case, the system is installed indoors and does
; not have any specialized housing.
; Configuration of the housing:
; Format: housing name, housing type,
; optional: high cube (True/False), housing azimuth in °, housing absorptivity, ground albedo
; default values for optional arguments = False,0,0.15,0.2
HOUSING =
    no_housing,NoHousing

; We now specify the type of HVAC to be installed in our BESS. In this case, the system is installed indoors and does
; not have any specialized HVAC unit.
; Configuration of the HVAC system:
; Format: HVAC system name, HVAC system type,
; optional: heating/cooling power in W, set-point temperature (in °C),
HVAC =
    no_hvac,NoHeatingVentilationAirConditioning

; We specify here that our BESS  consists of 1 DCSystem with a DC-DC Converter and a StorageTechnology
; Configuration of the DC storage system. Every AC system must have at least 1 DC system
; Format: AC-system name, DCDC converter name, storage technology name
STORAGE_SYSTEM_DC =
    ac_system_1,dcdc_1,technology_1

; We now set the capacity of our DC-DC Converter to 5 kW.
; Configuration of the DCDC converter
; Format: DCDC converter name, converter type, power in W, [optional: Efficiency in p.u.]
DCDC_CONVERTER =
    dcdc_1, NoLossDcDcConverter,5e3

; We now set the capacity of our Lithium-ion technology to 5 kWh.
; Configuration of the storage technology.
; Format: storage technology name, energy in Wh, technology type, [technology specific parameters]
; type: lithium-ion: Cell, optional: Start SOC, Start SOH
STORAGE_TECHNOLOGY =
    technology_1,5e3,lithium_ion,SonyLFP

; Configuration of power distributor logic between AC systems as well as between DC systems
POWER_DISTRIBUTOR_AC = EqualPowerDistributor
POWER_DISTRIBUTOR_DC = EqualPowerDistributor

; In this case, the system is installed indoors and a constant room temperature (= 25 °C) is assumed to be maintained.
; Configuration of ambient temperature for the simulation
;   ConstantAmbientTemperature,UserDefinedTemperatureValue (in °C): default value 25 °C, if unspecified
AMBIENT_TEMPERATURE_MODEL = ConstantAmbientTemperature,25

; In this case, the system is installed indoors and is not be exposed to direct sunlight.
; Configuration of solar irradiation model for the simulation
;   NoSolarIrradiationModel
SOLAR_IRRADIATION_MODEL = NoSolarIrradiationModel

; As we are still interested in the temperature evolution of our cells, we enable themral simulation.
; Thermal simulation enabled/disabled
; If False: NoHeatingVentilationAirConditioning and NoHousing must be selected
; If True: supports all Housing, HVAC, Ambient Temperature, and Solar Irradiation (LocationSolarIrradiationModel incompatible with NoHousing)
THERMAL_SIMULATION = True

; Configuration of Cycle Detector
CYCLE_DETECTOR = HalfCycleDetector

; This section configures the input profiles:
; POWER_PROFILE_DIR = Relative path to power profiles
;   LOAD_PROFILE = name of your load profile in the specified path
;   GENERATION_PROFILE = name of your PV generation profile in the specified path
; LOAD_PROFILE_SCALING = The input load profile is scaled to energy or power or no scaling at all (False)
;   LOAD_SCALING_FACTOR = If LOAD_PROFILE_SCALING = Energy --> Annual energy in Wh
; GENERATION_PROFILE_SCALING = The generation profile is scaled to power or no scaling at all (False)
;   GENERATION_SCALING_FACTOR = If GENERATION_PROFILE_SCALING = Power --> Peak power in W
[PROFILE]
POWER_PROFILE_DIR = profile/power/
LOAD_PROFILE = SBAP_Household_Profile
GENERATION_PROFILE = SBAP_PV_EEN_Power_Munich_2014

LOAD_PROFILE_SCALING = Energy
LOAD_SCALING_FACTOR = 3.5e6

GENERATION_PROFILE_SCALING = Power
GENERATION_SCALING_FACTOR = 10e3

