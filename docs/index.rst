.. simses_docu documentation master file, created by
   Bene Tepe

SimSES - Simulation of Stationary Energy Storage Systems
***********************************************************************************

Introduction to SimSES
=========================

SimSES is an advanced, modular simulation tool for stationary and mobile energy storage systems,
developed at the Technical University of Munich.
This tool is designed to carry out technical and economic assessments of different energy storage systems,
aiding in investment decisions and the design of optimized storage setups.
Its modular structure allows for customization to the specific requirements of both private users and business clients.
SimSES is particularly useful for applications such as frequency regulation, peak shaving, power boosting, enhancing self-consumption, and integrating vehicle-to-grid technologies.


.. figure:: images/SimSESGrafik_final.*
    :name: system-power-analysis









.. toctree::
   :maxdepth: 2
   :caption: User Guide

   aboutsimses
   quickstart
   extendedguide
   examples

.. toctree::
   :maxdepth: 2
   :caption: Technical Reference

   basics
   simbas
   applications


.. toctree::
   :maxdepth: 2
   :caption: Resources & Support

   faqs
   papers
   contact

